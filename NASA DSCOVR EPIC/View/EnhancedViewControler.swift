//
//  EnhancedViewControler.swift
//  NASA DSCOVR EPIC
//
//  Created by Alberto Téllez on 17/10/2019.
//  Copyright © 2019 albhyde. All rights reserved.
//

import UIKit

class EnhancedViewController: PrimaryEpicsViewController {
   
    @IBOutlet weak var enhancedTableView: UITableView!
    @IBOutlet weak var backImageEnhanced: UIImageView!
    
    var presenter: EnhancedPresenter?
    
    override func viewDidLoad() {
        
        Orientation().orientation(backImage: self.backImageEnhanced)
        
        tableView = enhancedTableView
        super.viewDidLoad()
        
        presenter?.viewDidLoad()
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { context in
            
            if self.backImageEnhanced != nil {
            
                Orientation().orientation(backImage: self.backImageEnhanced)  }
        })
    }
    
    override func getPresenter() -> EpicsPresenterProtocol? {
        
        return presenter
    }
}
