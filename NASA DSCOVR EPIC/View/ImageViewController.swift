//
//  ImageViewController.swift
//  NASA DSCOVR EPIC
//
//  Created by Alberto Téllez on 27/10/2019.
//  Copyright © 2019 albhyde. All rights reserved.
//

import UIKit
import CoreLocation

class ImageViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var captionLabl: UILabel!
    @IBOutlet weak var longitudLabel: UILabel!
    @IBOutlet weak var stackViewImage: UIStackView!
    @IBOutlet weak var upStackView: NSLayoutConstraint!
    @IBOutlet weak var distanceTxt: UILabel!
    @IBOutlet weak var upDistance: NSLayoutConstraint!
    @IBOutlet weak var distanceButton: UIButton!
    @IBOutlet weak var loadIndicator: UIActivityIndicatorView!
    @IBOutlet weak var upIndicatorView: NSLayoutConstraint!
    
    
    var epic: String?
    var date: String?
    var caption: String?
    var lat: Double?
    var lon: Double?
    var urlImage: URL?
    let locationManager = CLLocationManager()
    var distance: Double?
    var buttonSwitched : Bool = false
    let mygroup = DispatchGroup()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        distanceTxt.isHidden = true
        
        self.orientation()
        Orientation().orientation(backImage: self.backImage)
        
        dateLabel.text = date
        captionLabl.text = caption
        latitudeLabel.text = "latitude: " + String(format:"%f", lat!)
        longitudLabel.text = "longitud: " + String(format:"%f", lon!)
        
        if date!.isEmpty {
            
            dateLabel.text = Constants.errorData
            captionLabl.text = Constants.errorSorry
            
            return }
        
        let index: String.Index = ((date?.index(date!.startIndex, offsetBy: 10))!)
        let result = String((date?[..<index])!)
        
        if (epic?.contains("RGB"))! {
            urlImage = URL(string: Constants.nasaUrlArchive + "enhanced/" + result + "/png/" + epic! + ".png")
        } else  {
            urlImage = URL(string: Constants.nasaUrlArchive + "natural/" + result + "/png/" + epic! + ".png")
        }
        
        imageView.downloadImage(from: urlImage!, activityIndicator: loadIndicator)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        let coordinateNasa = CLLocation(latitude: lat!, longitude: lon!)
        let coordinateUser = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        let distanceInMeters = coordinateNasa.distance(from: coordinateUser)
        distanceTxt.text  = String(format: "%f", distanceInMeters)
        
    }
    
    @IBAction func distanceButton(_ sender: Any) {
        
        self.buttonSwitched = !self.buttonSwitched
        if self.buttonSwitched  {
            distanceTxt.isHidden = false
            self.locationManager.requestAlwaysAuthorization()
            self.locationManager.requestWhenInUseAuthorization()
            
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.startUpdatingLocation()
            }
        }
        else
        {
            distanceTxt.isHidden = true
            locationManager.stopUpdatingLocation()
        }
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { context in
            
            self.orientation()
            Orientation().orientation(backImage: self.backImage)
        })
    }
    
    func orientation() {
        
        if UIApplication.shared.statusBarOrientation.isLandscape {
            
            self.upStackView.constant = 8
            self.upDistance.constant = 320
            self.upIndicatorView.constant = 190
            
        } else {
            
            self.upStackView.constant = 90
            self.upDistance.constant = 395
            self.upIndicatorView.constant = 234
        }
        
    }
}

public extension UIImageView {
    
    func getData(from urlImage: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: urlImage, completionHandler: completion).resume()
    }
    
    func downloadImage(from urlImage: URL, activityIndicator: UIActivityIndicatorView?) {
        
        getData(from: urlImage) {
            data, response, error in
            guard let data = data, error == nil else {
                return
            }
            
            DispatchQueue.main.async() {
                
                self.image = UIImage(data: data)
                activityIndicator?.stopAnimating()
            }
        }
    }
}
