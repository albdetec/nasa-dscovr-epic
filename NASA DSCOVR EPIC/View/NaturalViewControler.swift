//
//  NaturalesViewControler.swift
//  NASA DSCOVR EPIC
//
//  Created by Alberto Téllez on 17/10/2019.
//  Copyright © 2019 albhyde. All rights reserved.
//

import UIKit
import FirebaseAuth

class NaturalViewController: PrimaryEpicsViewController {
    
    @IBOutlet weak var naturalTableView: UITableView!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var loginName: UITextField!
    @IBOutlet weak var loginEmail: UITextField!
    @IBOutlet weak var loginPassword: UITextField!
    @IBOutlet weak var loginScrollView: UIScrollView!
    @IBOutlet weak var backImage: UIImageView!
    
    var presenter: NaturalPresenter?
    let nickname = UserDefaults.standard
    
    override func viewDidLoad() {
        
        loginView.layer.cornerRadius = 9
        tableView = naturalTableView
        super.viewDidLoad()
        
        Orientation().orientation(backImage: self.backImage)
        
        enableTabBar(enableView: false)
        
        let second = nickname.string(forKey: "nickname")
        if second != nil  {
            enableTabBar(enableView: true)
        }
        
        presenter?.viewDidLoad()
    }
    
    override func getPresenter() -> EpicsPresenterProtocol? {
        return presenter
    }
    
    @IBAction func addUserButton(_ sender: Any) {
        
        let email = loginEmail.text
        let password = loginPassword.text
        
        if loginName.text!.isEmpty || loginName.text!.count < 4 {
            
            SharedClass.sharedInstance.alert(view: self, title: "Error!",
                                             message: "Nickname can´t be Empty or has less than four characters" )
        }
            
        else {
            
            nickname.set(loginEmail.text, forKey: "nickname")  }
        
        if let email = email, let password = password {
            
            Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
                self.userError(user: user, error: error)
            }
        }
    }
    
    @IBAction func loginButton(_ sender: Any) {
        
        let email = loginEmail.text
        let password = loginPassword.text
        if let email = email, let password = password {
            Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
                self.userError(user: user, error: error)
            }
        }
    }
    
    func userError (user: AuthDataResult?, error: Error?)  {
        
        if user != nil  {
            
            enableTabBar(enableView: true)
            nickname.set(loginEmail.text, forKey: "nickname")
            SharedClass.sharedInstance.alert(view: self, title: "Wellcome!", message: "I hope you enjoy!" )
        }
        
        if let error = error {
            
            SharedClass.sharedInstance.alert(view: self, title: "Error", message: error.localizedDescription)
        }
    }
    
    func enableTabBar(enableView: Bool) {
        
        let tabBarControllerItems = self.tabBarController?.tabBar.items
        
        if let tabArray = tabBarControllerItems {
            tabBarItem = tabArray[0]
            tabBarItem = tabArray[1]
            tabBarItem.isEnabled = enableView
            tabBarItem.isEnabled = enableView
        }
        
        self.loginScrollView.isHidden = enableView
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { context in
            
            Orientation().orientation(backImage: self.backImage)
        })
    }
}
