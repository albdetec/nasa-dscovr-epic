//
//  RootViewControler.swift
//  NASA DSCOVR EPIC
//
//  Created by Alberto Téllez on 17/10/2019.
//  Copyright © 2019 albhyde. All rights reserved.
//

import UIKit

protocol PrimaryEpicsViewProtocol {
    
    func showEpics(_ epics: [EpicData])
    func wasErrorLoadingData()
}

class PrimaryEpicsViewController: UIViewController, PrimaryEpicsViewProtocol {
    
    static let epicsCellIdentifier = "epicsCellIdentifier"
    
    var tableView: UITableView?
    var idEpics: [EpicData]?
    var originalIdEpics: [EpicData]?
    var epics = [String: EpicData]()
    var backgroundImage: UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView?.register(UINib(nibName: "EpicsTableViewCell", bundle: nil), forCellReuseIdentifier: PrimaryEpicsViewController.epicsCellIdentifier)
        
        tableView?.dataSource = self
        tableView?.delegate = self
    }
    
    func showEpics(_ epics: [EpicData]) {
        
        if let activityViewIndicator = view.viewWithTag(10) as? UIActivityIndicatorView {
            activityViewIndicator.stopAnimating()
        }
        
        idEpics = epics
        originalIdEpics = epics
        tableView?.reloadData()
    }
    
    func wasErrorLoadingData() {
        
        if presentedViewController == nil {
            
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            let alertDialog = UIAlertController(title: "Atención", message: Constants.errorData, preferredStyle: .alert)
            
            alertDialog.addAction(okAction)
            
            present(alertDialog, animated: true, completion: nil)
        }
    }
    
    func getPresenter() -> EpicsPresenterProtocol? { return nil }
}

extension PrimaryEpicsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return idEpics?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: PrimaryEpicsViewController.epicsCellIdentifier, for: indexPath) as? EpicsTableViewCell
        
        if let idEpic = idEpics?[indexPath.row] {
            
            cell?.configureCell(withEpic: idEpic)
            cell?.loadThumbs(withEpic: idEpic)
        }
        
            else  { cell?.clearCell() }
        
        return cell ?? EpicsTableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 200
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let idEpic = idEpics?[indexPath.row]
        
        let dateConverted = DateConverter().dateConverter(date: idEpic?.date)
        
        if let epic = idEpic?.image, let caption = idEpic?.caption, let lat = idEpic?.centroid_coordinates.lat,
            let lon = idEpic?.centroid_coordinates.lon, let navigationcontroller = navigationController {
            
            Router.router.routeToImageView(withEpic: epic, withDate: dateConverted,
                                           WithCaption: caption, WithLat: lat, WithLon: lon, navigationController: navigationcontroller)
        }
    }
}
