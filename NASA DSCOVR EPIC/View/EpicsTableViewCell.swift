//
//  EpicsTableViewCell.swift
//  NASA DSCOVR EPIC
//
//  Created by Alberto Téllez on 17/10/2019.
//  Copyright © 2019 albhyde. All rights reserved.
//

import UIKit

class EpicsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var identifierLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var imageLabel: UILabel!
    @IBOutlet weak var thumbImage: UIImageView!
    
    func configureCell(withEpic epic: EpicData) {
        
        identifierLabel.text = epic.identifier
        dateLabel.text = epic.date
        imageLabel.text = epic.image
    }
    
    func clearCell() {
        
        identifierLabel.text = ""
        dateLabel.text = ""
        imageLabel.text = ""
    }
    
    func loadThumbs(withEpic epic: EpicData)  {
        
        let activityIndicator = UIActivityIndicatorView()
        let dateConverted = DateConverter().dateConverter(date: epic.date)
        
        if dateConverted.isEmpty {
            
            dateLabel.text = Constants.errorData
            identifierLabel.text = Constants.errorSorry
            
            return }
        
        let index: String.Index = ((dateConverted.index(dateConverted.startIndex, offsetBy: 10)))
        let result = String((dateConverted[..<index]))
        let urlImage = URL(string: "https://epic.gsfc.nasa.gov/archive/natural/" + result + "/thumbs/epic_1b_" + epic.identifier + ".jpg")!
        
        thumbImage?.downloadImage(from: urlImage, activityIndicator: activityIndicator)
    }
}
