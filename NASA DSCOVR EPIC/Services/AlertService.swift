//
//  AlertService.swift
//  NASA DSCOVR EPIC
//
//  Created by Alberto Téllez on 02/11/2019.
//  Copyright © 2019 albhyde. All rights reserved.
//

import UIKit

class SharedClass: NSObject {
    
    static let sharedInstance = SharedClass()
    
    func alert(view: UIViewController, title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: { action in
        })
        alert.addAction(defaultAction)
        DispatchQueue.main.async(execute: {
            view.present(alert, animated: true)
        })
    }
}
