//
//  GlobalConstants.swift
//  NASA DSCOVR EPIC
//
//  Created by Alberto Téllez on 29/10/2019.
//  Copyright © 2019 albhyde. All rights reserved.
//

import Foundation

enum Constants {
    
    static let nasaKey = "5X1OdT2bmSHzs1U6rLyf81Zem1S0noFxgVNLi6WJ"
    static let nasaUrlApi = "https://api.nasa.gov/EPIC/api/"
    static let nasaUrlArchive = "https://epic.gsfc.nasa.gov/archive/"
    static let errorData = "Can´t get the data"
    static let errorSorry = "Sorry"
    static let backLandscape = "firmamentolandscape"
    static let backPortrait = "firmamento"
}
