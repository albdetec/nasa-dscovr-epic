//
//  Orientation.swift
//  NASA DSCOVR EPIC
//
//  Created by Alberto Téllez on 14/11/2019.
//  Copyright © 2019 albhyde. All rights reserved.
//

import Foundation
import UIKit


class Orientation {
    
    func orientation(backImage: UIImageView) {
        
        if UIApplication.shared.statusBarOrientation.isLandscape {
            
            SearchPath().searchPath(image: "firmamentolandscape", clase: "jpg", imageView: backImage)
            
        } else {
            
            SearchPath().searchPath(image: "firmamento", clase: "jpg", imageView: backImage)
        }
    }
}
