//
//  Orientation.swift
//  NASA DSCOVR EPIC
//
//  Created by Alberto Téllez on 07/11/2019.
//  Copyright © 2019 albhyde. All rights reserved.
//

import Foundation
import UIKit

class SearchPath: UIImage {
    
    func searchPath(image: String, clase: String, imageView: UIImageView) {
        let path =  Bundle.main.path(forResource: image, ofType: clase)
        if path != nil  {
            
            let imageFromPath = UIImage(contentsOfFile: path!)
            imageView.image = imageFromPath
        }
        
        return
    }
}
