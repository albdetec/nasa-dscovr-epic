//
//   DateConverter.swift
//  NASA DSCOVR EPIC
//
//  Created by Alberto Téllez on 28/10/2019.
//  Copyright © 2019 albhyde. All rights reserved.
//

import Foundation

class DateConverter {
    
    func dateConverter (date: String?) -> String {
        
        guard let dateString = date else { return "" }
        
        let dateStringFormatter = DateFormatter()
        dateStringFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        guard let dateFromString = dateStringFormatter.date(from: dateString) else { return "" }
        
        dateStringFormatter.dateFormat = "yyyy/MM/dd"
        let dateConverted = dateStringFormatter.string(from: dateFromString)
        
        return dateConverted
    }
}
