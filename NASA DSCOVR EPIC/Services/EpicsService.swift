//
//  EpicsService.swift
//  NASA DSCOVR EPIC
//
//  Created by Alberto Téllez on 17/10/2019.
//  Copyright © 2019 albhyde. All rights reserved.
//

import Foundation

enum EpicsType {
    
    case natural, enhanced
}

protocol EpicsServiceProtocol {
    
    func loadEpics(type: EpicsType,  success: @escaping ([EpicData]) -> Void, errorHandler: @escaping (_ errorDescription: String) -> Void)
    
}

final class EpicsService: EpicsServiceProtocol {
    
    static let epicsService = EpicsService()
    
    private init() {}
    
    func loadEpics(type: EpicsType, success: @escaping ([EpicData]) -> Void, errorHandler: @escaping (_ errorDescription: String) -> Void){
        
        var typeImage: String?
        
        switch type {
            
        case .natural:
            typeImage = "natural"
        case .enhanced:
            typeImage = "enhanced"
        }
        
        guard let typeEpicsPath = typeImage, let url = URL(string:
            Constants.nasaUrlApi
                + typeEpicsPath +
                "/images?api_key="
                + Constants.nasaKey) else { return }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            if let error = error {
                errorHandler(error.localizedDescription)
            }
            
            if let response = response as? HTTPURLResponse, response.statusCode == 200, let data = data {
                
                if let epics = try? JSONDecoder().decode([EpicInformation].self, from: data) {
                    
                    success(epics)
                }
            } else {
                
                errorHandler(Constants.errorData)
            }
            }.resume()
    }
}
