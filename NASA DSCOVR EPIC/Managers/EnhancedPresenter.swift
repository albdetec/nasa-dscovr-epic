//
//  EpicsEnhancedPresenter.swift
//  NASA DSCOVR EPIC
//
//  Created by Alberto Téllez on 17/10/2019.
//  Copyright © 2019 albhyde. All rights reserved.
//

import Foundation

class EnhancedPresenter: EpicsPresenterProtocol  {
    
    var view: PrimaryEpicsViewProtocol?
    var service: EpicsServiceProtocol?
    
    func viewDidLoad() {
        
        service?.loadEpics(type: .enhanced, success: { [weak self] (epics) in
            
            DispatchQueue.main.async {
                
                self?.view?.showEpics(epics)
            }
            }, errorHandler: { [weak self] error in
                
                DispatchQueue.main.async {
                    
                    self?.view?.wasErrorLoadingData()
                }
        })
    }
    
}
