//
//  PresenterProtocol.swift
//  NASA DSCOVR EPIC
//
//  Created by Alberto Téllez on 17/10/2019.
//  Copyright © 2019 albhyde. All rights reserved.
//

import Foundation

protocol EpicsPresenterProtocol {
    
    var view: PrimaryEpicsViewProtocol? { get}
    var service: EpicsServiceProtocol? { get }
    
    func viewDidLoad()
}
