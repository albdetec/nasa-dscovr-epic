//
//  Router.swift
//  NASA DSCOVR EPIC
//
//  Created by Alberto Téllez on 17/10/2019.
//  Copyright © 2019 albhyde. All rights reserved.
//

import Foundation
import UIKit

final class Router {
    
    static let router = Router()
    
    private init() {}
    
    func setUpNaturalViewController(_ viewController: NaturalViewController) {
        
        let epicService = EpicsService.epicsService
        let presenter = NaturalPresenter()
        presenter.service = epicService
        presenter.view = viewController
        viewController.presenter = presenter
    }
    
    func setUpEnhancedViewController(_ viewController: EnhancedViewController) {
        
        let epicService = EpicsService.epicsService
        let presenter = EnhancedPresenter()
        presenter.service = epicService
        presenter.view = viewController
        viewController.presenter = presenter
    }
    
    func routeToImageView(withEpic epic: String, withDate date: String, WithCaption caption: String, WithLat lat: Double, WithLon lon: Double, navigationController: UINavigationController) {
        
        if let imageViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "imageViewIdentifier") as? ImageViewController {
            
            imageViewController.epic = epic
            imageViewController.date = date
            imageViewController.caption = caption
            imageViewController.lat = lat
            imageViewController.lon = lon
            navigationController.pushViewController(imageViewController, animated: true)
        }
    }
}
